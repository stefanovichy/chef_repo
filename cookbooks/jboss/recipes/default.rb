#
# Cookbook:: jboss
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

apt_package "openjdk-7-jdk" do
  action :install
end

execute "download jboss and install" do
  command "wget #{node['jboss']['jboss_url']} ; unzip jboss-as-distribution-6.0.0.Final.zip ; mv jboss-6.0.0.Final #{node['jboss']['jboss_home']} ; rm /jboss-as-distribution-6.0.0.Final.zip" 
  action :run
  not_if { File.exist?('/usr/local/jboss/jboss-6.0.0.Final') }
end 

execute "deploy app" do
  command "wget #{node['application repo']} -P /usr/local/jboss/jboss-6.0.0.Final/server/default/deploy"
  action :run
  not_if { File.exist?('/usr/local/jboss/jboss-6.0.0.Final/server/default/deploy/sample.war') }
end

execute "add jdbc driver" do
  command "unzip /usr/local/jboss/jboss-6.0.0.Final/server/default/deploy/jboss-local-jdbc.rar ; mv jboss-local-jdbc.jar /usr/local/jboss/jboss-6.0.0.Final/server/default/lib"
  action :run
  not_if { File.exist?('/usr/local/jboss/jboss-6.0.0.Final/server/default/lib/jboss-local-jdbc.jar') }
end

#configure mysql jdbc datasource
execute "add mysql host ip to hosts" do
  command "echo #{node['mysql_hostname']} #{node['mysql_host_ip']} >> /etc/hosts"
  action :run
  not_if "cat /etc/hosts|grep #{node['mysql_hostname']}|grep #{node['mysql_host_ip']}"
end

template "/usr/local/jboss/jboss-6.0.0.Final/server/default/deploy/mysql-ds.xml" do
  source "jdbc_mysql.erb"
  mode "0755"
  owner "root"
  group "root"
end


#configure jboss to run as a service
template "/etc/init.d/jboss" do
  source "jboss_init.erb"
  mode "0755"
  owner "root"
  group "root"
end

#start jboss server
service "jboss" do
  action :start
end
