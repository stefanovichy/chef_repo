mysql_service node['mysql_hostname'] do
  port node['port']
  version '5.5'
  initial_root_password 'root123'
  action [:create, :start]
end
cookbook_file '/tmp/Sample-SQL-File-10-Rows.sql' do
  source 'Sample-SQL-File-10-Rows.sql'
  mode '0777'
  action :create
end
execute "import_dump" do
  command "mysql -S /run/mysql-Ubuntu_MySql/mysqld.sock -uroot -proot123 mysql < /tmp/Sample-SQL-File-10-Rows.sql"
  action :run
  not_if "mysql -S /run/mysql-Ubuntu_MySql/mysqld.sock -uroot -proot123 -e 'select * from mysql.user'"
end

